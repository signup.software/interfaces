import * as plugins from '../interfaces.plugins.js';
import * as data from '../data/index.js';

export interface IReq_GetPublicFormByFormToken
  extends plugins.typedrequestInterfaces.implementsTR<
    plugins.typedrequestInterfaces.ITypedRequest,
    IReq_GetPublicFormByFormToken
  > {
  method: 'getPublicFormByFormToken';
  request: {
    formToken: string;
  };
  response: {
    form: data.IPublicForm;
  };
}

export interface IReq_SendPublicFormEntry
  extends plugins.typedrequestInterfaces.implementsTR<
    plugins.typedrequestInterfaces.ITypedRequest,
    IReq_SendPublicFormEntry
  > {
  method: 'sendPublicFormEntry';
  request: {
    formEntry: data.IPublicFormEntry;
  };
  response: {
    formEntryValidation: data.IPublicFormEntryValidation;
  };
}

export interface IReq_GetFormEntries extends plugins.typedrequestInterfaces.implementsTR<
plugins.typedrequestInterfaces.ITypedRequest,
IReq_GetFormEntries
> {
  method: 'getFormEntries';
  request: {
    formToken: string;
    fromIncrementalId: string;
    limit: number;
  };
  response: {
    formEntries: data.IPublicFormEntry[]
  }
}
