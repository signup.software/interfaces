import * as plugins from '../interfaces.plugins.js';

export interface IPublicForm {
  formToken: string;
  dataFields: {
    name: string;
    mandatory: boolean;
    type: 'OptionSelector' | 'Checkbox' | 'MultiCheckbox' | 'String' | 'Number';
  }[];
}

export interface IPublicFormEntry {
  formToken: string;
  uniqueEntryId: string;
  status: IPublicFormEntryValidation['status'];
  dataFields: {
    name: string;
    mandatory: boolean;
    type: IPublicForm['dataFields'][0]['type'];
    value: string;
  }[];
}

export interface IPublicFormEntryValidation {
  formToken: string;
  uniqueEntryId: string;
  status: 'error' | 'ok' | 'idValidation';
  dataFields: {
    name: string;
    mandatory: boolean;
    type: IPublicForm['dataFields'][0]['type'];
    value: string;
    ok: boolean;
    status: string;
  }[];
}
