/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@signup.software/interfaces',
  version: '1.0.12',
  description: 'the public interfaces for signup.software'
}
